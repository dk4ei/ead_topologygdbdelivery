from pathlib import Path
import shutil
import datetime
import geopandas as gpd
import pandas as pd
import numpy as np

import arcpy

from ftp_connect.ftp_connect import FTPConnector

arcpy.env.overwriteOutput = True

BASEDIR = Path(__file__).resolve().parent.parent.parent
CLASS_CSV = BASEDIR.joinpath('data/classes_ruleID.csv')
GRID_PATH = BASEDIR.joinpath('data/OverallGrid/PN19032_ProjectGrid_UTM39N.shp')
PATH_TO_TILES = Path(r'\\TCMHarwell\Harwell\ProjectData\2019\PN19032_EAD_Habitat_Mapping\02_Processed_Data\WV_2014_HabitatMap\Habitat_Orthotile_Intersection').resolve()


class CopyTiles:
    """Class to download completed tiles from server to output location"""
    def __init__(self, TILES_CSV, TILES_DIR, remote=False):
        self.TILES_CSV = TILES_CSV
        self.TILES_DIR = TILES_DIR
        self.remote = remote #Change to True if remote, will then copy tiles from FTP
        self.ORTHOIDS = self.get_orthoids(self.TILES_CSV)
        if self.remote == False:
            print(self.remote)
            self.copy_tiles(self.TILES_DIR)
        else:
            self.remote_copy_tiles(self.TILES_DIR)

    def make_folders(func):
        def func_wrapper(self, *args, **kwargs):
            [self.TILES_DIR.joinpath(x).mkdir(parents=True) for x in self.ORTHOIDS if not self.TILES_DIR.joinpath(x).exists()]
            return func(self, *args)
        return func_wrapper

    @make_folders
    def copy_tiles(self, dir):
        """
        Reads csv and copies ORTHOIDs from server to dir.
        dir :: (Path/str) --> Path in which to save copied files. Will created a new folder (<orthoid>/) where files will be copied.
        """
        for orthoid in self.ORTHOIDS:
            dl_folder = dir.joinpath(f'{orthoid}')
            shps = [x for x in PATH_TO_TILES.joinpath(orthoid).iterdir() if x.name.startswith(f'{orthoid}.') if not x.name.endswith('.tif') if not x.name.endswith(f'.lock')]
            for shp in shps:
                if not dl_folder.joinpath(shp.name).exists():
                    try:
                        shutil.copy2(shp, dl_folder.joinpath(shp.name))
                    except PermissionError:
                        print(f'LOCKED - {shp.name}')

    @make_folders
    def remote_copy_tiles(self, dir):
        """
        Reads csv and copies ORTHOIDs from FTP server to remote dir.
        ***NOTE --> Only specify dir on local PC

        dir :: (Path/str) --> Path in which to save copied files. Will created a new folder (<orthoid>/) where files will be copied.
         ***NOTE --> Only specify dir on local PC
        """       
        username = "DKerr"
        password = "*TCM_hw19*"
        for orthoid in self.ORTHOIDS:
            dl_folder = dir.joinpath(f'{orthoid}')
            for suffix in ['shp', 'cpg', 'prj', 'dbf', 'shp.xml', 'shx']:
                if not dl_folder.joinpath(f'{orthoid}.{suffix}').exists():
                    x = FTPConnector(username, password)
                    x.download_file(f'Harwell/ProjectData/2019/PN19032_EAD_Habitat_Mapping/02_Processed_Data/WV_2014_HabitatMap/Habitat_Orthotile_Intersection/{orthoid}', dl_folder.joinpath(f'{orthoid}.{suffix}'))
                    x.close_connection()

    def get_orthoids(self, csv):
        """Returns list of orthoids in csv. Also makes folders in self.TILES_DIR. If tiles have already been downloaded, those orthoids are removed from the list.

        csv :: (Path/str) --> Path to orhoid csv

        Returns
        ORTHOIDS :: (list) --> list of ORTHOID strings
        """
        ORTHOIDS = pd.read_csv(csv)
        ORTHOIDS = ORTHOIDS['ORTHOID'].to_list()
        return ORTHOIDS

class Merge:
    """Class to merge all shapefiles in input dir"""
    def __init__(self, tiles_dir, merge_dir):
        self.tiles_dir = tiles_dir
        self.merge_dir = merge_dir
        self.merge_diss(self.tiles_dir, self.merge_dir)
        self.join_tables()

    def merge_diss(self, tiles_dir, merge_dir):
        """Main function to call functions to merge and dissolve tiles. Output merge will be named TerrestrialHabitat.shp
        
        tiles_dir :: (str) --> Path to dir holding tile folders
        merge_dir :: (str) --> Location to save TerrestrialHabitat
        """
        shps_to_merge = [str(y) for x in tiles_dir.iterdir() for y in x.iterdir() if y.name.endswith('.shp')]        
        if not merge_dir.exists():
            raise FileNotFoundError(f'{merge_folder} not found')
        else:
            arcpy.Merge_management(shps_to_merge, str(merge_dir.joinpath('tmp_merge.shp')))
            print('MERGED')
            arcpy.Dissolve_management(str(merge_dir.joinpath('tmp_merge.shp')), str(merge_dir.joinpath('TerrestrialHabitat.shp')), 'HabitatSub', "", "SINGLE_PART")
            print('DISSOLVED')
            arcpy.RepairGeometry_management(str(merge_dir.joinpath('TerrestrialHabitat.shp')), 'DELETE_NULL')
            print('REPAIRED')
            files_to_remove = [x for x in merge_dir.iterdir() if x.stem.endswith('merge')]
            files_to_remove += [x for x in merge_dir.iterdir() if x.name.endswith('_merge.shp.xml')]
            for i in files_to_remove:
                i.unlink()
            print('UNWANTED FILES DELETED')

    def join_tables(self):
        """Join TerrestrialHabitat.shp table with habitat class table"""       
        shp = self.merge_dir.joinpath('TerrestrialHabitat.shp')
        gdf = gpd.read_file(shp)
        gdf = gdf[['HabitatSub', 'geometry']].set_index('HabitatSub')
        df = pd.read_csv(CLASS_CSV).set_index('HabitatSub')
        missing_ids = list(set(gdf.index) - set(df.index))
        if missing_ids:
            arcpy.AddError(f'Error!!! THERE IS A TYPO IN THE HABITAT CLASS ID: {missing_ids}')
        gdf_joined = gdf.join(df)
        gdf_joined['Area_KM'] = gdf_joined['geometry'].area / 10**6
        gdf_joined['Area_HA'] = gdf_joined['geometry'].area / 10**4
        gdf_joined = gdf_joined.reset_index()
        gdf_joined['ID'] = gdf_joined.index
        cols = ['ID', 'HabitatTyp', 'HabitatT_1','HabitatSub', 'HabitatS_1', 'RuleID', 'Area_KM', 'Area_HA','MMU_HA', 'geometry']
        gdf_joined = gdf_joined[cols]
        gdf_joined['MMU_valid'] = np.where(gdf_joined['Area_HA'] < gdf_joined['MMU_HA'], 'False', 'True')
        gdf_joined.to_file(shp)
        sr = arcpy.SpatialReference(32639)
        arcpy.DefineProjection_management(str(shp), sr)
        arcpy.AddMessage(f"{self.merge_dir.joinpath('TerrestrialHabitat.shp')} is ready to have topology checked.")

class CheckTopology:
    """Class to import shapefile into specified GDB (wgs84), check topology, and export error shapefile to specified path"""
    def __init__(self, merge_shp, gdb, error_dir):
        self.merge_shp = merge_shp
        self.gdb = gdb
        self.error_dir = error_dir
        self.dissolve(self.merge_shp)
        self.join_tables()
        self.import_to_gdb(self.merge_shp, self.gdb)
        self.export_errors(self.gdb, self.error_dir)

    def dissolve(self, merge_shp):
        """Dissolve all adjoining poylgons"""
        tmp_shp = merge_shp.parent.joinpath('tmp.shp')
        arcpy.Dissolve_management(str(merge_shp), str(tmp_shp), 'HabitatSub', "", "SINGLE_PART")
        arcpy.RepairGeometry_management(str(tmp_shp), 'DELETE_NULL')
        ####COPY TMP TO MERGE
        arcpy.Copy_management(str(tmp_shp), str(merge_shp))
        files_to_remove = [x for x in merge_shp.parent.iterdir() if x.stem.endswith('tmp')]
        files_to_remove += [x for x in merge_shp.parent.iterdir() if x.name.endswith('tmp.shp.xml')]
        for i in files_to_remove:
            i.unlink()

    def join_tables(self):
        """Rejoin dissolved tables to classification table and calculate area"""
        """Join TerrestrialHabitat.shp table with habitat class table"""       
        shp = self.merge_shp
        gdf = gpd.read_file(shp)
        gdf = gdf[['HabitatSub', 'geometry']].set_index('HabitatSub')
        df = pd.read_csv(CLASS_CSV).set_index('HabitatSub')
        missing_ids = list(set(gdf.index) - set(df.index))
        if missing_ids:
            arcpy.AddError(f'Error!!! THERE IS A TYPO IN THE HABITAT CLASS ID: {missing_ids}')
        gdf_joined = gdf.join(df)
        gdf_joined['Area_KM'] = gdf_joined['geometry'].area / 10**6
        gdf_joined['Area_HA'] = gdf_joined['geometry'].area / 10**4
        gdf_joined = gdf_joined.reset_index()
        gdf_joined['ID'] = gdf_joined.index
        cols = ['ID', 'HabitatTyp', 'HabitatT_1','HabitatSub', 'HabitatS_1', 'RuleID', 'Area_KM', 'Area_HA','MMU_HA', 'geometry']
        gdf_joined = gdf_joined[cols]
        gdf_joined['MMU_valid'] = np.where(gdf_joined['Area_HA'] < gdf_joined['MMU_HA'], 'False', 'True')
        gdf_joined.to_file(shp)
        sr = arcpy.SpatialReference(32639)
        arcpy.DefineProjection_management(str(shp), sr)

    def import_to_gdb(self, shp, gdb):
        """Imports to gdb and checks topology"""
        #Create dataset in which to store Terrestrial Habitat
        feature_dataset = str(gdb.joinpath('HabitatMapping'))
        if not arcpy.Exists(feature_dataset):
            arcpy.CreateFeatureDataset_management(str(gdb), 'HabitatMapping', arcpy.SpatialReference(4326))
        arcpy.env.workspace = arcpy.env.scratchWorkspace = str(gdb.joinpath('HabitatMapping'))
        arcpy.env.overwriteOutput = True
        topology_layer = str(gdb.joinpath('HabitatMapping/HabitatMapping_Topology'))
        if arcpy.Exists(topology_layer):
            #delete topology
            if 'TerrestrialHabitat' in arcpy.Describe(topology_layer).featureClassNames:
                arcpy.RemoveFeatureClassFromTopology_management(topology_layer,  'TerrestrialHabitat')
        else:
            #remove previous feature class from topology and add new
            arcpy.CreateTopology_management(arcpy.env.workspace, 'HabitatMapping_Topology') #, in_cluster_tolerance=0.0001
            print('topology created')
        features_classes_to_delete = arcpy.ListFeatureClasses()
        if features_classes_to_delete:
            for feature in features_classes_to_delete:
                arcpy.Delete_management(feature)
        arcpy.FeatureClassToGeodatabase_conversion(str(shp), str(gdb.joinpath('HabitatMapping')))
        aliases = ['ID', 'Habitat Type Number', 'Habitat Type', 'Habitat Sub Type Number', 'Habitat Sub Type', 'Rule ID', 'Area Kilometers', 'Area Hectares', 'MMU Hectares']
        fields = [ 'ID', 'HabitatTyp', 'HabitatT_1', 'HabitatSub', 'HabitatS_1', 'RuleID', 'Area_KM', 'Area_HA', 'MMU_HA']

        #add field aliases
        for i, j in zip(fields, aliases):
            arcpy.AlterField_management('TerrestrialHabitat', i, new_field_alias=j)
        arcpy.AddFeatureClassToTopology_management(topology_layer, 'TerrestrialHabitat', 1, 1)
        arcpy.AddRuleToTopology_management(topology_layer, "Must Not Have Gaps (Area)", str(gdb.joinpath('TerrestrialHabitat')))
        arcpy.AddRuleToTopology_management(topology_layer, "Must Not Overlap (Area)", str(gdb.joinpath('TerrestrialHabitat')))

        #Validate topology
        arcpy.ValidateTopology_management(topology_layer)
        print('TOPOLOGY CHECKED')
        arcpy.ExportTopologyErrors_management(topology_layer, arcpy.env.workspace, 'ERRORS')
        arcpy.AddMessage('Topology validated')

    def export_errors(self, gdb, error_dir):
        """Export topological errors to shapefiles in error_dir"""
        arcpy.env.workspace = arcpy.env.scratchWorkspace = str(gdb.joinpath('HabitatMapping'))
        error_features = arcpy.ListFeatureClasses(wild_card="ERROR*")
        arcpy.FeatureClassToShapefile_conversion(error_features, str(error_dir))
        for i in error_features:
            sr = arcpy.SpatialReference(32639)
            arcpy.DefineProjection_management(f'{str(error_dir.joinpath(i))}.shp', sr)
        arcpy.AddMessage(f'Topology errors have been exported to {str(error_dir)}')


    


