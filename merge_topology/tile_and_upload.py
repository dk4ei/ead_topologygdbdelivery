"""Module to tile terrestrial habitat shp into constituent tiles and push to server. Previous version of tile on server will be backed up to folder with today's date."""
from pathlib import Path
import arcpy 
import datetime
import geopandas as gpd 
import pandas as pd
import shutil


BASEDIR = Path(__file__).resolve().parent.parent.parent
FULL_COUNTRY_TILE = BASEDIR.joinpath('data/OverallGrid/EmirateWideGrid.shp')
TILE = FULL_COUNTRY_TILE.parent.joinpath('clipped_tile.shp')
PATH_TO_TILES = Path(r'\\TCMHarwell\Harwell\ProjectData\2019\PN19032_EAD_Habitat_Mapping\02_Processed_Data\WV_2014_HabitatMap\Habitat_Orthotile_Intersection').resolve()


class TileTerrHabitat:
    def __init__(self, shp, staging_folder):
        """
        shp :: (Path/str) --> Path to Terrestrial Merge to be tiled
        staging_folder:: (Path/str) --> Path to dir in which to hold tiled shapefiles before pushing to server as backup.
        """
        self.shp = shp
        self.staging_folder = staging_folder
        self.clip_tiles(self.shp)
        self.clip_hab_to_tiles()
        self.move_shps_to_server(PATH_TO_TILES, self.staging_folder)

    def clip_tiles(self, shp):
        """Clip Emirate-wide grid to Terrestrial Habitat shapefile"""
        if TILE.exists():
            [x.unlink() for x in TILE.parent.iterdir() if x.name.startswith(TILE.stem)] 
        tile_hab_intersection = TILE.parent.joinpath('int.shp')
        if tile_hab_intersection.exists():
            [x.unlink() for x in TILE.parent.iterdir() if x.name.startswith(tile_hab_intersection.stem)]        
        arcpy.Intersect_analysis([str(shp), str(FULL_COUNTRY_TILE)], str(tile_hab_intersection))
        arcpy.Dissolve_management(str(tile_hab_intersection), str(TILE), 'ORTHOID')
        [x.unlink() for x in TILE.parent.iterdir() if x.name.startswith(tile_hab_intersection.stem)]

    def clip_hab_to_tiles(self):
        """Clip TerrestrialHabitat to each cell of TILES"""
        gdf_tile = gpd.read_file(TILE)
        gdf_hab = gpd.read_file(self.shp)
        for index, row in gdf_tile.iterrows():
            outfolder = self.staging_folder.joinpath(f'{row["ORTHOID"]}')
            out_shp = outfolder.joinpath(f"{row['ORTHOID']}.shp")
            gdf_ = gdf_tile[gdf_tile['ORTHOID'] == row['ORTHOID']]
            gdf_int = gpd.overlay(gdf_hab, gdf_, how='intersection')
            gdf_int['Area_KM'] = gdf_int['geometry'].area / 10**6
            gdf_int['Area_HA'] = gdf_int['geometry'].area / 10**4
            gdf_int['Id'] = gdf_int['ID']
            gdf_int['OrthoID'] = gdf_int['ORTHOID']
            cols = [ 'OrthoID','Id', 'HabitatTyp', 'HabitatT_1', 'HabitatSub', 'HabitatS_1', 'RuleID', 'Area_KM', 'Area_HA', 'MMU_HA', 'geometry']
            gdf_int = gdf_int[cols]
            if not outfolder.exists():
                outfolder.mkdir(parents=True, exist_ok=True)
                gdf_int.to_file(outfolder.joinpath(f'multi.shp'))
                sr = arcpy.SpatialReference(32639)
                arcpy.DefineProjection_management(str(outfolder.joinpath(f'multi.shp')), sr)
                arcpy.MultipartToSinglepart_management(str(outfolder.joinpath(f'multi.shp')), str(out_shp))
                gdf = gpd.read_file(out_shp)
                gdf['Area_KM'] = gdf['geometry'].area / 10**6
                gdf['Area_HA'] = gdf['geometry'].area / 10**4
                gdf['Id'] = gdf.index
                gdf.to_file(out_shp)
                sr = arcpy.SpatialReference(32639)
                arcpy.DefineProjection_management(str(out_shp), sr)
            [x.unlink() for x in out_shp.parent.iterdir() if x.name.startswith('multi')]

    def move_shps_to_server(self, HARWELL, TILES):
        """Copy tiled shapefiles to server and move previous version to superseded folder

        HARWELL :: (Path/str) --> Path to habitat tiles root folder
        TILES :: (Path/str) --> Path to local staging folder to be moved to server
        """

        for orthoid in TILES.iterdir():
            files_to_copy = [x for x in orthoid.iterdir()]
            mv_folder = HARWELL.joinpath(f'{orthoid.name}')
            now = str(datetime.datetime.now()).split(' ')[0]
            superseded_folder = mv_folder.joinpath(now)
            if not superseded_folder.exists(): #This will not back up tiles more than once in a day. If they need to be backed up multiple times in a day, the superseded folder should be deleted.
                superseded_folder.mkdir(parents=True, exist_ok=True)
                files_to_move = [x for x in mv_folder.iterdir() if x.name.startswith(f'{orthoid.name}.') if not 'TIF' in x.name if not x.name.startswith(f'{orthoid.name}.tif')  if not x.name.endswith('lock')]
                for i in files_to_move:
                    shutil.move(str(i), str(superseded_folder))
            for j in files_to_copy:
                shutil.copy(str(j), str(mv_folder))
            print(f'{orthoid.name} copied')