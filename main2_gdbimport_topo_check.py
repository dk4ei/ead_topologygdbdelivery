from pathlib import Path

from .merge_topology.merge_topo_checker import CheckTopology

BASEDIR = Path(__file__).resolve().parent.parent

MERGE_DIR = BASEDIR.joinpath('data/merge') #Location in which to save merged/dissolved tiles ready for topology check


GDB_DIR = BASEDIR.joinpath('EAD_HabitatMapping.gdb') # GDB in which to do topology
ERROR_DIR = BASEDIR.joinpath('data/errors')

def main(merge_shp, gdb, error_dir):
    CheckTopology(merge_shp, gdb, error_dir)

if __name__ == "__main__":
    merge_shp = MERGE_DIR.joinpath('TerrestrialHabitat.shp')
    gdb = GDB_DIR
    error_dir = ERROR_DIR
    main(merge_shp, gdb, error_dir)