"""
Module to upload or download files from the 4EI FTP in binary format
"""

from ftplib import FTP
import ftplib
from pathlib import Path

class FTPConnector:

	def __init__(self, username, password, ftp_path="87.127.209.178"):
		"""
		Initialisation to connect to server. FTP path is option, and defaults to 4EI server root
		"""
		self.username = username
		self.password = password
		self.ftp_path = ftp_path #Defaults to 4EI FTP root dir
		self.ftp = self.get_connection()

	def __call__(self):
		l = self.ftp.retrlines("LIST")
		print(l)
		print(self.ftp.pwd())

	def get_connection(self):
		"""Returns connection to ftp"""
		ftp = FTP()
		ftp.connect(self.ftp_path, 31)
		ftp.login(self.username, self.password)
		return ftp

	def list_dir(self, path):
		"""Prints list of contents of path
		Parameters:
		path:: (str) --> Path to move within FTP. Will then print list of files within that dir
		"""
		self.ftp.cwd(path)
		l = self.ftp.retrlines("LIST")
		print(l)
		self.ftp.cwd(self.ftp_path) #RETURNS to root dir

	def close_connection(self):
		"""Closes connection to FTP"""
		self.ftp.quit()

	def upload_file(self, local_path, upload_path):
		"""Uploads file to the FTP

		Parameters:
		local_path :: (str/Path) --> Path to local file to upload
		upload_path :: (str) --> Path to location to upload on the server (If a file exists with the same name, it will be overwritten)
		"""
		self.ftp.cwd(upload_path)
		#l = self.ftp.retrlines("LIST")
		with open(local_path, 'rb') as f:
			self.ftp.storbinary(f'STOR {local_path.name}', f)	


	def download_file(self, ftp_path, download_path):
		"""Downloads file to local
		
		Parameters:
		ftp_path :: (str) --> Location on server of file to download
		download_path :: (Path/str) --> Local location to write file
		"""
		self.ftp.cwd(ftp_path)
		try:
			with open(download_path, 'wb') as f:
				self.ftp.retrbinary(f'RETR {download_path.name}', f.write)
		except ftplib.error_perm:
			print(f'{download_path.name} not on the server')
			


if __name__ == "__main__":
	BASEDIR = Path(__file__).resolve().parent.parent
	local_path = BASEDIR.joinpath('WV_C02_L17_test_folder/test.txt')
	upload_path = "Harwell/ProjectData/2019/PN19032_EAD_Habitat_Mapping/02_Processed_Data/WV_2014_HabitatMap/Habitat_Orthotile_Intersection/WV_C02_L17_test_folder"
	ftp_path = "87.127.209.178"
	username = "DKerr"
	password = "*TCM_hw19*"
	
	"""UPLOAD FILES"""	
	for f in [x for x in BASEDIR.joinpath('WV_C02_L17_test_folder').iterdir() if x.name.startswith('WV_C02_L17.') if not x.name.endswith('tif')]:
		x =  FTPConnector(username, password, ftp_path)
		x.upload_file(f, upload_path)
		print(f.name)
		x.close_connection()

	"""DOWNLOAD FILES"""
	for suffix in ['shp', 'cpg', 'prj', 'dbf', 'shp.xml', 'shx']:
		x = FTPConnector(username, password, ftp_path)
		x.download_file('Harwell/ProjectData/2019/PN19032_EAD_Habitat_Mapping/02_Processed_Data/WV_2014_HabitatMap/Habitat_Orthotile_Intersection/WV_C02_L17_test_folder', BASEDIR.joinpath(f'WV_C02_L17.{suffix}'))
		x.close_connection()
	