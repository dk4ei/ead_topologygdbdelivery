from pathlib import Path 
from .merge_topology.tile_and_upload import TileTerrHabitat

def main(shp, staging_folder):
    TileTerrHabitat(shp, staging_folder)


if __name__ == "__main__":
    BASEDIR = Path(__file__).resolve().parent.parent 
    shp = BASEDIR.joinpath('data/merge/TerrestrialHabitat.shp')
    staging_folder = BASEDIR.joinpath('data/tiles_out')
    main(shp, staging_folder)