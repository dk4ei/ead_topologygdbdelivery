from pathlib import Path

from .merge_topology.merge_topo_checker import CopyTiles, Merge, CheckTopology

BASEDIR = Path(__file__).resolve().parent.parent


def main(csv, tiles, merge_dir, remote_connection):
    #COPY TILES TO MERGE
    CopyTiles(csv, tiles, remote=remote_connection) # Change remote=True if not in 4EI office
    #MERGE TILES
    Merge(tiles, merge_dir)

if __name__ == "__main__":
    #############################################################
    ##################  VARIABLES TO BE SET #####################
    TILES_CSV = BASEDIR.joinpath('orthoids.csv') #List of all completed tiles to be downloaded (This should inculde ALL tiles completed, not just tiles for current delivery)

    TILES_DIR = BASEDIR.joinpath('data/tiles_in') #Location to download completed tiles

    MERGE_DIR = BASEDIR.joinpath('data/merge') #Location in which to save merged/dissolved tiles ready for topology check

    ##################  VARIABLES TO BE SET #####################
    #############################################################
    main(TILES_CSV, TILES_DIR, MERGE_DIR, False)


